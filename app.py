"""
TP2 Jasmin Martel
"""
import mysql.connector
import os
import secrets
from flask import Flask, render_template, request, redirect, abort, flash
from flask.logging import create_logger
from flask_babel import Babel

import bd
from compte import bp_compte
from objet import bp_objet

app = Flask(__name__)
app.register_blueprint(bp_compte, url_prefix='/compte')
app.register_blueprint(bp_objet, url_prefix='/objet')
app.secret_key = secrets.token_hex()

logger = create_logger(app)

app.config["BABEL_DEFAULT_LOCALE"] = "fr_CA"
app.config['MORCEAUX_VERS_OBJETS'] = ["static", "images", "objets"]
app.config['ROUTE_VERS_OBJETS'] = "/".join(app.config['MORCEAUX_VERS_OBJETS'])
app.config['CHEMIN_VERS_OBJETS'] = os.path.join(
    app.instance_path.replace("instance", ""),
    *app.config['MORCEAUX_VERS_OBJETS']
)

babel = Babel(app)


def get_locale():
    """Retourne la locale à utiliser"""
    return app.config["BABEL_DEFAULT_LOCALE"]


@app.route('/')
def index():
    with bd.creer_connexion() as conn:
        troques_liste = bd.get_objets_limit(conn)
        if not troques_liste:
            abort(500)

    return render_template("index.jinja", troques_liste=troques_liste)


@app.route('/regions', methods=["GET"])
def set_locale():
    """Change la locale à utiliser"""
    region = request.args.get("region", default="fr_CA")
    if not region:
        abort(400, "Incapable de chercher la région sélectionné")
    app.config["BABEL_DEFAULT_LOCALE"] = region
    flash("Votre région a été reconfiguré")
    return redirect("/", 303)


@app.errorhandler(400)
def mauvaise_requete(e):
    """Pour les erreurs 400"""
    logger.exception(e)

    return render_template(
        'erreur.jinja',
        message="une erreur est sourvenu dans les paramètres GET ou POST" + e.description
    ), 400


@app.errorhandler(401)
def pas_identifie(e):
    """Pour les erreurs 401, quand l'utilisateur n'est pas identifié"""
    logger.exception(e)

    return render_template(
        'erreur.jinja',
        message="Vous devez être identifié pour faire cela \n" +
        "<a href='/compte/authentification'>Connectez-vous maintenant</a>"
    )


@app.errorhandler(403)
def interdit(e):
    """Pour les erreurs 403, quand l'utilisateur n'est pas autorisé"""
    logger.exception(e)

    return render_template('erreur.jinja', message="Vous n'avez pas l'autorisation d'accèder à ceci" )


@app.errorhandler(404)
def inexistant(e):
    """Pour les erreurs 404"""
    logger.exception(e)

    return render_template(
        'erreur.jinja',
        message=e.description
    ), 404


@app.errorhandler(500)
def erreur_serveur_interne(e):
    """Pour les erreurs 500"""
    logger.exception(e)

    message = "Une erreur s'est produite avec notre serveur. Désolé"

    if type(e) is mysql.connector.Error:
        message = "Une erreur s'est produite avec la base de données. Désolé"

    return render_template(
        'erreur.jinja',
        message=message + e.description
    ), 500
