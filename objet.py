import secrets
import os
from flask import Blueprint, render_template, request, redirect, abort, session, flash
from datetime import date
from babel import dates
import bd
import utilitaires

bp_objet = Blueprint('objet', __name__)


@bp_objet.route('/liste_objets')
def liste_objets():
    with bd.creer_connexion() as conn:
        troques_liste = bd.get_objets(conn)
        if not troques_liste:
            abort(500)
    return render_template("objet/liste_objets.jinja", troques_liste=troques_liste)


@bp_objet.route('/details/<int:id_objet>')
def details(id_objet):
    from app import get_locale

    with bd.creer_connexion() as conn:
        resultat = bd.get_objet(conn, id_objet)
        if not resultat:
            abort(400)
        resultat['date_creation'] = dates.format_date(resultat['date_creation'], locale=get_locale())
        nom_detenteur = bd.get_utilisateur_nom(conn, resultat['id_utilisateur'])
        if not nom_detenteur:
            abort(400)
    return render_template('objet/details.jinja', troque=resultat, detenteur=nom_detenteur['nom'])


@bp_objet.route('/ajouter_objet', methods=["GET", "POST"])
def ajouter_objet():
    """Affiche la page d'ajout d'objet et traite la requête."""
    if "utilisateur" not in session:
        abort(401)
    if request.method == "GET":
        return render_template("objet/ajout_objet.jinja")

    if request.method == "POST":
        # region initialisation
        class_titre = ""
        class_description = ""
        class_image = ""
        class_categorie = ""
        erreur_titre = ""
        erreur_description = ""
        erreur_image = ""
        erreur_categorie = ""
        # endregion
        with bd.creer_connexion() as conn:
            titre = request.form.get("titre", default="")
            description = request.form.get("description", default="")
            fichier_image = request.files["image"]
            categorie = request.form.get("categorie", default="")
            categories = bd.get_categories(conn)

            # region Validations
            if not 1 <= len(titre) <= 50:
                class_titre = "is-invalid"
                erreur_titre = "Le titre doit être entre 1 et 50 caractères"

            if not 5 <= len(description) <= 2000:
                class_description = "is-invalid"
                erreur_description = "la description doit être entre 5 et 2000 caractères"

            if not fichier_image:
                class_image = ""
                erreur_image = ""
                cheminimage = "images_default.png"
            elif not 6 <= len(fichier_image.filename) <= 50:
                class_image = "is-invalid"
                erreur_image = "Le nom doit être entre 6 et 50 caractères, incluant le . et l'extension"
            else:
                extension = fichier_image.filename.rsplit('.', 1)[1].lower()
                cheminimage = secrets.token_hex(16) + '.' + extension
                images = bd.get_objets_images(conn)
                while any(image["photo"] == cheminimage for image in images):
                    cheminimage = secrets.token_hex(16) + '.' + extension

            if not any(categorie == cat['description'] for cat in categories) or not categorie:
                class_categorie = "is-invalid"
                erreur_categorie = "Entrer une catégorie existante"
            # endregion

            # Sauvegarde de l'image
            if not cheminimage == "images_default.png":
                from app import app
                cheminimagecomplet = os.path.join(
                    app.config['CHEMIN_VERS_OBJETS'], cheminimage
                )
                fichier_image.save(cheminimagecomplet)

            if not class_titre and not class_description and not class_image and not class_categorie:
                date_creation = date.today()
                id_categorie = bd.get_categorie(conn, categorie)
                if not id_categorie:
                    abort(500)
                # Ajoute l'objet à la bd
                bd.add_objet(conn, titre, description, cheminimage, id_categorie['id'], date_creation, session["utilisateur"][0])
                flash("L'ajout a été fait avec succès")
                return redirect("/", code=303)

        return render_template(
            "objet/ajout_objet.jinja",
            titre=titre,
            description=description,
            image=fichier_image.filename,
            categorie=categorie,
            class_titre=class_titre,
            class_description=class_description,
            class_image=class_image,
            class_categorie=class_categorie,
            erreur_titre=erreur_titre,
            erreur_description=erreur_description,
            erreur_image=erreur_image,
            erreur_categorie=erreur_categorie
        )


@bp_objet.route('/details/<int:id_objet>/modifier_objet', methods=["GET", "POST"])
def modifier_objet(id_objet):
    """Affiche la page de modification d'objet et traite la requête"""
    # region initialisation
    class_titre = ""
    class_description = ""
    class_image = ""
    class_categorie = ""
    erreur_titre = ""
    erreur_description = ""
    erreur_image = ""
    erreur_categorie = ""
    # endregion
    if "utilisateur" not in session:
        abort(401)
    with bd.creer_connexion() as conn:
        objet = bd.get_objet(conn, id_objet)
        if objet['id_utilisateur'] is not session["utilisateur"][0] and session["utilisateur"][3] is not 1:
            abort(403)
        nom_categorie = bd.get_categorie(conn, objet["categorie"])
        if not nom_categorie:
            abort(400)
        if request.method == "GET":
            return render_template(
                "objet/modifier_objet.jinja",
                id_objet=objet["id"],
                titre=objet["titre"],
                description=objet["description"],
                image=objet["photo"],
                categorie=nom_categorie["description"]
            )
        if request.method == "POST":
            titre = request.form.get("titre", default="")
            description = request.form.get("description", default="")
            fichier_image = request.files["image"]
            categorie = request.form.get("categorie", default="")
            categories = bd.get_categories(conn)

            # region Validations
            if not 1 <= len(titre) <= 50:
                class_titre = "is-invalid"
                erreur_titre = "Le titre doit être entre 1 et 50 caractères"

            if not 5 <= len(description) <= 2000:
                class_description = "is-invalid"
                erreur_description = "la description doit être entre 5 et 2000 caractères"

            if not any(categorie == cat['description'] for cat in categories) or not categorie:
                class_categorie = "is-invalid"
                erreur_categorie = "Entrer une catégorie existante"

            if fichier_image:
                if not 6 <= len(fichier_image.filename) <= 50:
                    class_image = "is-invalid"
                    erreur_image = "Le nom doit être entre 6 et 50 caractères, incluant le . et l'extension"
                else:
                    extension = fichier_image.filename.rsplit('.', 1)[1].lower()
                    cheminimage = secrets.token_hex(16) + '.' + extension
                    images = bd.get_objets_images(conn)
                    while any(image["photo"] == cheminimage for image in images):
                        cheminimage = secrets.token_hex(16) + '.' + extension
                    # Sauvegarde de l'image
                    from app import app
                    cheminimagecomplet = os.path.join(
                        app.config['CHEMIN_VERS_OBJETS'], cheminimage
                    )
            else:
                cheminimage = objet["photo"]
            # endregion

            if not class_titre and not class_description and not class_image and not class_categorie:
                id_categorie = bd.get_categorie(conn, categorie)
                if not id_categorie:
                    abort(400)
                # modifie l'objet à la bd
                bd.update_objet(conn, objet["id"], titre, description, cheminimage, id_categorie['id'])
                # Revérifie si l'image a été changée
                if cheminimagecomplet:
                    fichier_image.save(cheminimagecomplet)
                    if objet["photo"] is not "images_default.png":
                        utilitaires.supprimer_image(objet["photo"])
                flash("La modification a été fait avec succès")
                return redirect("/", code=303)

        return render_template(
            "objet/modifier_objet.jinja",
            id_objet=objet["id"],
            titre=titre,
            description=description,
            image=fichier_image.filename,
            categorie=categorie,
            class_titre=class_titre,
            class_description=class_description,
            class_image=class_image,
            class_categorie=class_categorie,
            erreur_titre=erreur_titre,
            erreur_description=erreur_description,
            erreur_image=erreur_image,
            erreur_categorie=erreur_categorie
        )


@bp_objet.route('/troque/<int:id_objet>', methods=["GET"])
def troquer_objet(id_objet):
    """Troque l'objet sélectionné avec un objet de votre choix dans votre collection"""
    if "utilisateur" not in session:
        abort(401)
    print(session["utilisateur"])
    with bd.creer_connexion() as conn:
        objet_troque = bd.get_objet(conn, id_objet)
        if not objet_troque:
            abort(404)
        print(objet_troque)
        if objet_troque["id_utilisateur"] == session["utilisateur"][0]:
            abort(400)
        print("pas à moi")
        id_choisi = request.args.get('objet_troque')
        if id_choisi:
            bd.switch_objet(
                conn,
                session["utilisateur"][0],
                objet_troque["id_utilisateur"],
                id_choisi,
                objet_troque["id"]
            )
            flash("Le troque a été fait avec succès")
            return redirect('/', code=303)
        objets_utilisateur = bd.get_objets_utilisateur(conn, session['utilisateur'][0])
    return render_template("objet/troque.jinja", objets=objets_utilisateur, objet_troque=objet_troque)
