from flask import Blueprint, render_template, request, redirect, abort, session, flash

import bd
import utilitaires
from utilitaires import hacher_mdp
bp_compte = Blueprint('compte', __name__)


@bp_compte.route('authentification', methods=['GET', 'POST'])
def authentification():
    """ Connexion d'un utilisateur """
    erreur = False
    courriel = request.form.get("courriel", default="")

    if request.method == 'POST':
        mdp = hacher_mdp(request.form.get("mdp"))
        with bd.creer_connexion() as conn:
            utilisateur = bd.authentifier(conn, courriel, mdp)
            erreur =(not utilisateur)
            if not erreur:
                session["utilisateur"] = utilisateur["id"], utilisateur["nom"], utilisateur["courriel"], \
                    utilisateur["admin"]
                session.permanent = True
                flash("Connexion réussie")
                return redirect('/', code=303)

    return render_template(
        'compte/authentifier.jinja',
        courriel=courriel,
        erreur=erreur
    )


@bp_compte.route('/creation', methods=['GET', 'POST'])
def creation():
    """Création d'un utilisateur"""

    champs = {
        "courriel": {
            "valeur": request.form.get("courriel", default=""),
            "en_erreur": False
        },
        "nom": {
            "valeur": request.form.get("nom", default=""),
            "en_erreur": False
        },
        "mdp": {
            "en_erreur": False
        },
    }

    if request.method == 'POST':
        if not utilitaires.validation_email(champs["courriel"]["valeur"]):
            champs["courriel"]["en_erreur"] = True
        mdp = request.form.get("mdp")
        if not utilitaires.validation_password(mdp):
            champs["mdp"]["en_erreur"] = True
        if mdp != request.form.get("mdp2"):
            champs["mdp"]["en_erreur"] = True
        else:
            mdp = hacher_mdp(mdp)
            if not champs["courriel"]["en_erreur"] == True and not champs["nom"]["en_erreur"] == True and not champs["mdp"]["en_erreur"]:
                with bd.creer_connexion() as conn:
                    utilisateur = {
                        "courriel": champs["courriel"]["valeur"],
                        "nom": champs["nom"]["valeur"],
                        "mdp": mdp
                    }

                    utilisateur["id"] = bd.add_utilisateur(conn, utilisateur)
                    session["utilisateur"] = utilisateur["id"], utilisateur["nom"], utilisateur["courriel"], 0
                    session.permanent = True
                    flash("Création du compte faite. Connexion réussie")
                    return redirect('/', code=303)

    return render_template('compte/creer.jinja', champs=champs)


@bp_compte.route('/deconnecter', methods=['GET'])
def deconnecter():
    """Pour se déconnecter"""
    session.clear()
    flash("Vous avez été déconnecté")
    return redirect("/")


@bp_compte.route('/ajouter_utilisateur', methods=["GET", "POST"])
def ajouter_utilisateur():
    """Pour ajouter un utilisateur quand on est admin"""
    if "utilisateur" not in session:
        abort(401)
    if session["utilisateur"][3] == 0:
        abort(403)
    champs = {
        "courriel": {
            "valeur": request.form.get("courriel", default=""),
            "en_erreur": False
        },
        "nom": {
            "valeur": request.form.get("nom", default=""),
            "en_erreur": False
        },
        "mdp": {
            "en_erreur": False
        },
    }

    if request.method == 'POST':
        if not utilitaires.validation_email(champs["courriel"]["valeur"]):
            champs["courriel"]["en_erreur"] = True
        mdp = request.form.get("mdp")
        if not utilitaires.validation_password(mdp):
            champs["mdp"]["en_erreur"] = True
        if mdp != request.form.get("mdp2"):
            champs["mdp"]["en_erreur"] = True
        else:
            mdp = hacher_mdp(mdp)
        if not champs["courriel"]["en_erreur"] == True and not champs["nom"]["en_erreur"] == True and not champs["mdp"]["en_erreur"] == True:
            with bd.creer_connexion() as conn:
                utilisateur = {
                    "courriel": champs["courriel"]["valeur"],
                    "nom": champs["nom"]["valeur"],
                    "mdp": mdp
                }

                bd.add_utilisateur(conn, utilisateur)
                flash("L'ajout de l'utilisateur a été fait avec succès")
                return redirect('/', code=303)

    return render_template('compte/creer.jinja', champs=champs)


@bp_compte.route('/liste_utilisateurs')
def liste_utilisateurs():
    """Pour voir tous les utilisateurs quand on est admin"""
    if "utilisateur" not in session:
        abort(401)
    if session["utilisateur"][3] == 0:
        abort(403)
    with bd.creer_connexion() as conn:
        utilisateurs = bd.get_utilisateurs(conn)
    return render_template("compte/liste_utilisateurs.jinja", utilisateurs=utilisateurs)


@bp_compte.route('/liste_utilisateurs/supprimer/<int:id_utilisateur>')
def supprimer_utilisateur(id_utilisateur):
    """Sypprime l'utilisateur cliqué"""
    if "utilisateur" not in session:
        abort(401)
    if session["utilisateur"][3] == 0:
        abort(403)
    with bd.creer_connexion() as conn:
        bd.delete_utilisateur(conn, id_utilisateur)
        flash("Le compte a été supprimé avec succès")
        return redirect('/compte/liste_utilisateurs', code=303)
