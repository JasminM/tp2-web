"""
Le modèle
Connexion à la BD
"""
import types
import contextlib
import mysql.connector


@contextlib.contextmanager
def creer_connexion():
    """Pour créer une connexion à la BD"""
    conn = mysql.connector.connect(
        user="root",
        password="",
        host="127.0.0.1",
        database="tp1_troqueur",
        raise_on_warnings=True
    )

    # Pour ajouter la méthode getCurseur() à l'objet connexion
    conn.get_curseur = types.MethodType(get_curseur, conn)

    try:
        yield conn
    except Exception:
        conn.rollback()
        raise
    else:
        conn.commit()
    finally:
        conn.close()


@contextlib.contextmanager
def get_curseur(self):
    """Permet d'avoir les enregistrements sous forme de dictionnaires"""
    curseur = self.cursor(dictionary=True)
    try:
        yield curseur
    finally:
        curseur.close()


def get_categories(conn):
    with conn.get_curseur() as curseur:
        curseur.execute('SELECT * FROM categories')
        return curseur.fetchall()


def get_categorie(conn, categorie):
    with conn.get_curseur() as curseur:
        if isinstance(categorie, int):
            curseur.execute(f"""SELECT description FROM categories WHERE id={categorie}""")
        elif isinstance(categorie, str):
            curseur.execute('SELECT id FROM categories WHERE description=%(categorie)s',
                            {
                                "categorie": categorie
                            })
        return curseur.fetchone()


# region objet
def get_objets_limit(conn):
    """Récupère tout des objets ordonnés en ordre décoissant de l'id avec un limit de 5."""
    with conn.get_curseur() as curseur:
        curseur.execute('SELECT * FROM objets ORDER BY id DESC LIMIT 5')
        return curseur.fetchall()


def get_objets(conn):
    """Récupère tout des objets ordonnés en ordre décoissant de l'id."""
    with conn.get_curseur() as curseur:
        curseur.execute('SELECT * FROM objets ORDER BY id DESC')
        return curseur.fetchall()


def get_objets_utilisateur(conn, id_utilisateur):
    with conn.get_curseur() as curseur:
        curseur.execute(f"""SELECT * FROM objets WHERE id_utilisateur={id_utilisateur}""")
        return curseur.fetchall()


def get_objet(conn, id_objet):
    with conn.get_curseur() as curseur:
        curseur.execute(f"""SELECT * FROM objets WHERE id={id_objet}""")
        return curseur.fetchone()


def add_objet(conn, titre, description, image, id_categorie, date_creation, id_utilisateur):
    with conn.get_curseur() as curseur:
        curseur.execute(
            'INSERT INTO objets (titre, description, photo, categorie, date_creation, id_utilisateur) VALUES (%(titre)s, %(description)s, %(image)s, %(categorie)s, %(date)s, %(id_utilisateur)s)',
            {
                "titre": titre,
                "description": description,
                "image": image,
                "categorie": id_categorie,
                "date": date_creation,
                "id_utilisateur": id_utilisateur
            })


def get_objets_images(conn):
    with conn.get_curseur() as curseur:
        curseur.execute("SELECT photo FROM objets")
        return curseur.fetchall()


def update_objet(conn, id_objet, le_titre, description, image, categorie):
    """Modifie un objet dans la BD"""
    with conn.get_curseur() as curseur:
        curseur.execute('UPDATE objets SET titre=%(titre)s, description=%(description)s, photo=%(image)s, categorie=%(categorie)s WHERE id=%(id_objet)s',
                        {
                            "titre": le_titre,
                            "description": description,
                            "image": image,
                            "categorie": categorie,
                            "id_objet": id_objet
                        })


def switch_objet(conn, id_utilisateur, id_utilisateur_troque, id_objet_utilisateur, id_objet_troque):
    with conn.get_curseur() as curseur:
        curseur.execute(
            f"""UPDATE objets
            SET id_utilisateur={id_utilisateur}
            WHERE id={id_objet_troque}"""
        )
        curseur.execute(
            f"""UPDATE objets
            SET id_utilisateur={id_utilisateur_troque}
            WHERE id={id_objet_utilisateur}"""
        )
# endregion


# region utilisateur
def authentifier(conn, courriel, mdp):
    """Retourne un utilisateur avec le courriel et le mdp"""
    with conn.get_curseur() as curseur:
        curseur.execute(
            "SELECT * FROM utilisateurs WHERE courriel=%(courriel)s and mdp=%(mdp)s",
            {
                "courriel": courriel,
                "mdp" : mdp
            }
        )
        return curseur.fetchone()


def add_utilisateur(conn, utilisateur):
    """Ajoute un utilisateur dans la BD"""
    with conn.get_curseur() as curseur:
        curseur.execute(
            "INSERT INTO utilisateurs (courriel, nom, mdp) VALUES (%(courriel)s, %(nom)s, %(mdp)s)",
            utilisateur
        )
        return curseur.lastrowid


def get_utilisateur_nom(conn, id_utilisateur):
    """Va chercher le nom de l'utilisateur"""
    with conn.get_curseur() as curseur:
        curseur.execute(
            f"""SELECT nom FROM utilisateurs WHERE id={id_utilisateur}"""
        )
        return curseur.fetchone()


def get_utilisateurs(conn):
    """Retourne tous les utilisateurs"""
    with conn.get_curseur() as curseur:
        curseur.execute(f"""SELECT * FROM utilisateurs""")
        return curseur.fetchall()


def delete_utilisateur(conn, id_utilisateur):
    """Supprime l'utilisateur voulu"""
    with conn.get_curseur() as curseur:
        curseur.execute(f"""DELETE FROM utilisateurs WHERE id={id_utilisateur}""")
        return curseur.lastrowid
# endregion

