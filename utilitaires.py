"""
Classe de méthodes utilitaires
"""
import hashlib
import os
import re


def supprimer_image(image_nom):
    """Supprime l'image dans le dossier static"""
    from app import app
    chemin_image_complet = os.path.join(
        app.config['CHEMIN_VERS_OBJETS'], image_nom
    )
    try:
        os.remove(chemin_image_complet)
        print(f"L'image {image_nom} a été supprimée avec succès.")
    except FileNotFoundError:
        print(f"L'image {image_nom} n'a pas été trouvée.")
    except Exception as e:
        print(f"Une erreur s'est produite lors de la suppression de l'image {image_nom}: {e}")


def validation_email(email):
    email_regex = re.compile(r'^\S+@\S+\.\S+$')
    return bool(re.match(email_regex, email))


def validation_password(password):
    password_regex = re.compile(r'^(?=.*[A-Z])(?=.*[a-z])(?=.*\d).{8,}$')
    return bool(re.match(password_regex, password))


def hacher_mdp(mdp_en_clair):
    """Prend un mot de passe en clair et lui applique une fonction de hachage"""
    return hashlib.sha512(mdp_en_clair.encode('utf-8')).hexdigest()
